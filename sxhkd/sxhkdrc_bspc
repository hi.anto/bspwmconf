#!/bin/bash

# Quit bspwm
# =============================================================================
#
super + alt + q
	bspc quit

# Close [or kill] currently focused node
# =============================================================================
#
super + {_,shift +}q
    bspc node {-c,-k}

# Focus Monitor 
# =============================================================================
#

# focus the monitor in the given direction
super + {period,comma}
    bspc monitor -f {east,west}
    
# move the focused node to monitor in given direction
super + shift {period,comma}
    bspc node -m {east,west} --follow

# Focus/swap nodes/desktop
# =============================================================================
#

# focus the node in the given direction
super + {h,j,k,l}
    bspc node -f {west,south,north,east}

# Swap focused window with the one in the given direction.
super + ctrl + {h,j,k,l}
    bspc node -s {west,south,north,east}

# super + <number> = focus the given desktop
# super + shift + <number> = send focused window to given desktop
super + {_,shift +}{0-9}
    bspc {desktop -f,node -d} {0-9}

# With `alt` focus {next,previous} window in the current desktop.  With
# `super` focus {next,previous} desktop in the current monitor.
# Cyclical behaviour.  Move in the opposite direction while also holding
# down `shift`.
{super,alt} + {_,shift +} Tab
    bspc {desktop,node} -f {next,prev}.local

# Switch to last active {desktop,node}.  With super move to the last
# active desktop.  With alt move to the last active node. "grave" is
# positioned above Tab in QWERTY layouts.
{super,alt} + grave
    bspc {desktop,node} -f last

# Expand or contract node in the given direction.
#
# For floating windows one can use Alt + Right Mouse click to
# drag/resize in the given direction (this works in other DEs as well,
# such as MATE and Xfce).  I have not set the script to resize floating
# windows because: (i) I do not use them and if I do I do not want to
# change their dimensions, and (ii) to keep the script concise.
#super + ctrl + {h,j,k,l}
#    /home/hianto/bin/own_script_bspwm_node_resize {west,south,north,east} 10

# Move a floating window with the arrow keys (else use Alt + Left Mouse
# Click to drag).  I seldom use floating windows.
super + shift + {Left,Down,Up,Right}
    bspc node -v {-5 0,0 5,0 -5,5 0}

# Swap current window with the biggest one in the present
# desktop.
super + shift + b
    bspc node -s biggest.local

# Preselect the direction or insert again to cancel the preselection.
# This enters the manual tiling mode that splits the currently focused
# window.
super + alt + {h,j,k,l}
    bspc node --presel-dir '~{west,south,north,east}'

# Preselect the ratio.  The default value is 0.5, defined in `bspwmrc`.
super + alt + {1-9}
    bspc node -o 0.{1-9}

# Node layout state and flags
# =============================================================================
#
#
# Toggle monocle layout (maximise focused node).  If you use `-l
# monocle` you lose the toggle functionality.
super + m
    bspc desktop -l next

# Toggle tiled, pseudo_tiled, floating, fullscreen view.  Tiled is what
# is used by default.  Pseudo-tiled presents the window in its original
# dimensions inside the split it would normally occupy as a tile (I have
# never found a practical use for this).  Floating windows have their
# own dimensions and can be stacked on top of each other.
#
# NOTE the presence of the tilde ~.  If current state matches the given
# state, then the argument is interpreted as the last state.
super + {t,shift + space,space,f}
    bspc node -t ~{tiled,pseudo_tiled,floating,fullscreen}

# Set the node flags.  Locked is a node that cannot be closed with `bspc
# node --close`.  Marked is a specific selector that allows operating on
# the given item (such as sending the marked item to the newest
# preselection).  Sticky stays in the focused desktop.  Private tries to
# keep its dimensions and tiling position.
super + shift + a ; {l,s,p}
    bspc node -g {locked,sticky,private}

# Continuous input mode
# =============================================================================
#
#
# EXPLAIN: The following key chords have a colon sign `:' before their
# last part.  By pressing the keys to the left of the colon, you can
# then pass the commands by only using the keys to the right of the
# colon.  This eliminates the need for repeated chord presses that
# involve the modifier keys.  Exit the mode with "Escape".
#
# NOTE: All of the following are mirrors of their afore-mentioned
# couterparts.

# Resize node in the given direction.
super + c : {h,j,k,l}
    /home/hianto/bin/own_script_bspwm_node_resize {west,south,north,east} 10

# Mode for dynamic gaps.
super + c : bracket{left,right}
    bspc config -d focused window_gap "$(($(bspc config -d focused window_gap) {-,+} 5 ))"

# Move floating windows with the arrow keys
super + c : {Left,Down,Up,Right}
    bspc node -v {-5 0,0 5,0 -5,5 0}

